import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import App from './App';
import TodoItem from './components/TodoItem';
import { getTodos } from './API';
import { act } from 'react-dom/test-utils';
import { shallow, mount } from 'enzyme';
import AddTodo from './components/AddTodo';

test('render should have texts', () => {
  render(<App />);
  const header = screen.getByText(/My Todos/i);
  expect(header).toBeInTheDocument();

  const button = screen.getByText(/Add Todo/i);
  expect(button).toBeInTheDocument();
});

test('click Add Todo button', () => {
  const mockSaveTodo = jest.fn(e => e.preventDefault());
  render(<AddTodo saveTodo={mockSaveTodo} />);
  fireEvent.click(screen.getByText(/Add Todo/i));

  expect(mockSaveTodo).toHaveBeenCalledTimes(1);
});
test('unable to click uncomplete TodoItem COMPLETE button', () => {
  const mockHandleUpdateTodo = jest.fn();
  const mockHandleDeleteTodo = jest.fn();
  const todo: ITodo = {
    _id: '5fa6aa98093539728c42bbed',
    name: 'todo name',
    status: true,
    createdAt: '2020-11-07T14:09:28.581Z',
    updatedAt: '2020-11-07T14:14:35.461Z',
  };
  render(
    <TodoItem
      key={todo._id}
      todo={todo}
      updateTodo={mockHandleUpdateTodo}
      deleteTodo={mockHandleDeleteTodo}
    />,
  );
  const completeButton = screen.getByText(/COMPLETE/i);
  fireEvent.click(completeButton);

  expect(mockHandleUpdateTodo).toHaveBeenCalledTimes(0);
});

test('click uncomplete TodoItem COMPLETE button', () => {
  const mockHandleUpdateTodo = jest.fn();
  const mockHandleDeleteTodo = jest.fn();
  const todo: ITodo = {
    _id: '5fa6aa98093539728c42bbed',
    name: 'todo name',
    status: false,
    createdAt: '2020-11-07T14:09:28.581Z',
    updatedAt: '2020-11-07T14:14:35.461Z',
  };
  render(
    <TodoItem
      key={todo._id}
      todo={todo}
      updateTodo={mockHandleUpdateTodo}
      deleteTodo={mockHandleDeleteTodo}
    />,
  );
  const completeButton = screen.getByText(/COMPLETE/i);
  fireEvent.click(completeButton);

  expect(mockHandleUpdateTodo).toHaveBeenCalledTimes(1);
});

test('click TodoItem DELETE button', () => {
  const mockHandleUpdateTodo = jest.fn();
  const mockHandleDeleteTodo = jest.fn();
  const todo: ITodo = {
    _id: '5fa6aa98093539728c42bbed',
    name: 'todo name',
    status: true,
    createdAt: '2020-11-07T14:09:28.581Z',
    updatedAt: '2020-11-07T14:14:35.461Z',
  };
  render(
    <TodoItem
      key={todo._id}
      todo={todo}
      updateTodo={mockHandleUpdateTodo}
      deleteTodo={mockHandleDeleteTodo}
    />,
  );
  const deleteButton = screen.getByText(/DELETE/i);
  fireEvent.click(deleteButton);

  expect(mockHandleDeleteTodo).toHaveBeenCalledTimes(1);
});

// test('getTodos api', async () => {
//   await act(async () => {
//     // return getTodos();
//     const { data } = await getTodos();
//   });
//   // console.log(a);

//   // expect(Array.isArray(data.todos)).toBeTruthy();
// });

test('render completed TodoItem', () => {
  const mockHandleUpdateTodo = jest.fn(e => e.preventDefault());
  const mockHandleDeleteTodo = jest.fn(e => e.preventDefault());
  const todo: ITodo = {
    _id: '5fa6aa98093539728c42bbed',
    name: 'todo name',
    status: true,
    createdAt: '2020-11-07T14:09:28.581Z',
    updatedAt: '2020-11-07T14:14:35.461Z',
  };
  render(
    <TodoItem
      key={todo._id}
      todo={todo}
      updateTodo={mockHandleUpdateTodo}
      deleteTodo={mockHandleDeleteTodo}
    />,
  );

  const todoName = screen.getByText(/todo name/i);
  expect(todoName).toBeInTheDocument();
  expect(todoName).toHaveStyle({ textDecoration: 'line-through' });

  const completeButton = screen.getByText(/COMPLETE/i);
  expect(completeButton.closest('button')).toBeDisabled();

  const deleteButton = screen.getByText(/DELETE/i);
  expect(deleteButton.closest('button')).toBeEnabled();
});

test('render uncompleted TodoItem', () => {
  const mockHandleUpdateTodo = jest.fn(e => e.preventDefault());
  const mockHandleDeleteTodo = jest.fn(e => e.preventDefault());
  const todo: ITodo = {
    _id: '5fa6aa98093539728c42bbed',
    name: 'todo name',
    status: false,
    createdAt: '2020-11-07T14:09:28.581Z',
    updatedAt: '2020-11-07T14:14:35.461Z',
  };
  render(
    <TodoItem
      key={todo._id}
      todo={todo}
      updateTodo={mockHandleUpdateTodo}
      deleteTodo={mockHandleDeleteTodo}
    />,
  );

  const todoName = screen.getByText(/todo name/i);
  expect(todoName).toBeInTheDocument();
  expect(todoName).toHaveStyle({ textDecoration: '' });

  const completeButton = screen.getByText(/COMPLETE/i);
  expect(completeButton.closest('button')).toBeEnabled();

  const deleteButton = screen.getByText(/DELETE/i);
  expect(deleteButton.closest('button')).toBeEnabled();
});
