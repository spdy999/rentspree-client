import React, { useState } from 'react';
import { Paper, makeStyles, createStyles } from '@material-ui/core';

type Props = {
  saveTodo: (e: React.FormEvent, formData: ITodo | any) => void;
};

const AddTodo: React.FC<Props> = ({ saveTodo }) => {
  const [formData, setFormData] = useState<ITodo | {}>();

  const handleForm = (e: React.FormEvent<HTMLInputElement>): void => {
    setFormData({
      ...formData,
      [e.currentTarget.id]: e.currentTarget.value,
    });
  };
  const useStyles = makeStyles(theme =>
    createStyles({
      paper: {
        padding: theme.spacing(1),
        margin: theme.spacing(1),
      },
    }),
  );
  const classes = useStyles();

  return (
    <form className="Form" onSubmit={e => saveTodo(e, formData)}>
      <Paper className={classes.paper}>
        <input placeholder="TODO" onChange={handleForm} type="text" id="name" />
        <button>Add Todo</button>
      </Paper>
    </form>
  );
};

export default AddTodo;
