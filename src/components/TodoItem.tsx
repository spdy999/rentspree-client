import React from 'react';
import {
  Paper,
  makeStyles,
  createStyles,
  Button,
  Box,
  Typography,
} from '@material-ui/core';

type Props = TodoProps & {
  updateTodo: (todo: ITodo) => void;
  deleteTodo: (_id: string) => void;
};

const Todo: React.FC<Props> = ({ todo, updateTodo, deleteTodo }) => {
  const useStyles = makeStyles(theme =>
    createStyles({
      paper: {
        padding: theme.spacing(1),
        margin: theme.spacing(1),
      },
      checkTodo: {
        textDecoration: todo.status ? `line-through` : '',
      },
      space: {
        margin: theme.spacing(1),
      },
    }),
  );
  const classes = useStyles();
  return (
    <Paper className={classes.paper}>
      <Box component="div" display="inline">
        <h1 className={classes.checkTodo}>{todo.name}</h1>
      </Box>
      <Box component="div" display="inline">
        <Button
          className={classes.space}
          disabled={todo.status}
          onClick={() => updateTodo(todo)}
          variant="contained"
          color="primary"
        >
          Complete
        </Button>
        <Button
          onClick={() => deleteTodo(todo._id)}
          variant="contained"
          color="secondary"
        >
          Delete
        </Button>
      </Box>
    </Paper>
  );
};

export default Todo;
